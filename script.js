let tabsContent = document.querySelectorAll('[data-tab-content]');


let tabs = document.querySelectorAll('[data-target]');

tabs.forEach(tab =>{
    tab.addEventListener('click',() => {
        tabs.forEach(tab =>{
            tab.classList.remove('active');
        });
        let target = document.getElementById(tab.dataset.target);
        target.classList.add('active');
        tabsContent.forEach(tabContent => {
            tabContent.classList.remove('active');

        });
        let targetContent = document.getElementById(tab.dataset.target.slice(1,tab.dataset.target.length));
        targetContent.classList.add('active');
    })});

const _CONTAINERS = Object.freeze([
    'Graphic Design',
    'Web Design',
    'Landing Pages',
    'Wordpress'
]);

const imageList = Object.freeze([
    {
        imageSource: "./img/web%20design/web-design1.jpg",
        title: "CREATIVE DESIGN",
        category: _CONTAINERS[Math.floor(Math.random() * (_CONTAINERS.length))]
    },
    {
        imageSource: "./img/web%20design/web-design2.jpg",
        title: "CREATIVE DESIGN",
        category: _CONTAINERS[Math.floor(Math.random() * (_CONTAINERS.length))]
    },
    {
        imageSource: "./img/web%20design/web-design3.jpg",
        title: "CREATIVE DESIGN",
        category: _CONTAINERS[Math.floor(Math.random() * (_CONTAINERS.length))]
    },
    {
        imageSource: "./img/web%20design/web-design4.jpg",
        title: "CREATIVE DESIGN",
        category: _CONTAINERS[Math.floor(Math.random() * (_CONTAINERS.length))]
    },
    {
        imageSource: "./img/web%20design/web-design5.jpg",
        title: "CREATIVE DESIGN",
        category: _CONTAINERS[Math.floor(Math.random() * (_CONTAINERS.length))]
    },
    {
        imageSource: "./img/web%20design/web-design6.jpg",
        title: "CREATIVE DESIGN",
        category: _CONTAINERS[Math.floor(Math.random() * (_CONTAINERS.length))]
    },
    {
        imageSource: "./img/web%20design/web-design7.jpg",
        title: "CREATIVE DESIGN",
        category: _CONTAINERS[Math.floor(Math.random() * (_CONTAINERS.length))]
    },

    {
        imageSource: "./img/graphic%20design/graphic-design1.jpg",
        title: "CREATIVE DESIGN",
        category: _CONTAINERS[Math.floor(Math.random() * (_CONTAINERS.length))]
    },
    {
        imageSource: "./img/graphic%20design/graphic-design2.jpg",
        title: "CREATIVE DESIGN",
        category: _CONTAINERS[Math.floor(Math.random() * (_CONTAINERS.length))]
    },
    {
        imageSource: "./img/graphic%20design/graphic-design3.jpg",
        title: "CREATIVE DESIGN",
        category: _CONTAINERS[Math.floor(Math.random() * (_CONTAINERS.length))]
    },
    {
        imageSource: "./img/graphic%20design/graphic-design4.jpg",
        title: "CREATIVE DESIGN",
        category: _CONTAINERS[Math.floor(Math.random() * (_CONTAINERS.length))]
    },
    {
        imageSource: "./img/graphic%20design/graphic-design5.jpg",
        title: "CREATIVE DESIGN",
        category: _CONTAINERS[Math.floor(Math.random() * (_CONTAINERS.length))]
    },
    {
        imageSource: "./img/graphic%20design/graphic-design6.jpg",
        title: "CREATIVE DESIGN",
        category: _CONTAINERS[Math.floor(Math.random() * (_CONTAINERS.length))]
    },
    {
        imageSource: "./img/graphic%20design/graphic-design7.jpg",
        title: "CREATIVE DESIGN",
        category: _CONTAINERS[Math.floor(Math.random() * (_CONTAINERS.length))]
    },
    {
        imageSource: "./img/graphic%20design/graphic-design8.jpg",
        title: "CREATIVE DESIGN",
        category: _CONTAINERS[Math.floor(Math.random() * (_CONTAINERS.length))]
    },
    {
        imageSource: "./img/graphic%20design/graphic-design9.jpg",
        title: "CREATIVE DESIGN",
        category: _CONTAINERS[Math.floor(Math.random() * (_CONTAINERS.length))]
    },
    {
        imageSource: "./img/graphic%20design/graphic-design10.jpg",
        title: "CREATIVE DESIGN",
        category: _CONTAINERS[Math.floor(Math.random() * (_CONTAINERS.length))]
    },
    {
        imageSource: "./img/graphic%20design/graphic-design11.jpg",
        title: "CREATIVE DESIGN",
        category: _CONTAINERS[Math.floor(Math.random() * (_CONTAINERS.length))]
    },
    {
        imageSource: "./img/graphic%20design/graphic-design12.jpg",
        title: "CREATIVE DESIGN",
        category: _CONTAINERS[Math.floor(Math.random() * (_CONTAINERS.length))]
    },
    {
        imageSource: "./img/landing%20page/landing-page1.jpg",
        title: "CREATIVE DESIGN",
        category: _CONTAINERS[Math.floor(Math.random() * (_CONTAINERS.length))]
    },
    {
        imageSource: "./img/landing%20page/landing-page2.jpg",
        title: "CREATIVE DESIGN",
        category: _CONTAINERS[Math.floor(Math.random() * (_CONTAINERS.length))]
    },
    {
        imageSource: "./img/landing%20page/landing-page3.jpg",
        title: "CREATIVE DESIGN",
        category: _CONTAINERS[Math.floor(Math.random() * (_CONTAINERS.length))]
    },
    {
        imageSource: "./img/landing%20page/landing-page4.jpg",
        title: "CREATIVE DESIGN",
        category: _CONTAINERS[Math.floor(Math.random() * (_CONTAINERS.length))]
    },
    {
        imageSource: "./img/landing%20page/landing-page5.jpg",
        title: "CREATIVE DESIGN",
        category: _CONTAINERS[Math.floor(Math.random() * (_CONTAINERS.length))]
    },
    {
        imageSource: "./img/landing%20page/landing-page6.jpg",
        title: "CREATIVE DESIGN",
        category: _CONTAINERS[Math.floor(Math.random() * (_CONTAINERS.length))]
    },
    {
        imageSource: "./img/landing%20page/landing-page7.jpg",
        title: "CREATIVE DESIGN",
        category: _CONTAINERS[Math.floor(Math.random() * (_CONTAINERS.length))]
    },
    {
        imageSource: "./img/wordpress/wordpress1.jpg",
        title: "CREATIVE DESIGN",
        category: _CONTAINERS[Math.floor(Math.random() * (_CONTAINERS.length))]
    },
    {
        imageSource: "./img/wordpress/wordpress2.jpg",
        title: "CREATIVE DESIGN",
        category: _CONTAINERS[Math.floor(Math.random() * (_CONTAINERS.length))]
    },
    {
        imageSource: "./img/wordpress/wordpress3.jpg",
        title: "CREATIVE DESIGN",
        category: _CONTAINERS[Math.floor(Math.random() * (_CONTAINERS.length))]
    },
    {
        imageSource: "./img/wordpress/wordpress4.jpg",
        title: "CREATIVE DESIGN",
        category: _CONTAINERS[Math.floor(Math.random() * (_CONTAINERS.length))]
    },
    {
        imageSource: "./img/wordpress/wordpress5.jpg",
        title: "CREATIVE DESIGN",
        category: _CONTAINERS[Math.floor(Math.random() * (_CONTAINERS.length))]
    },
    {
        imageSource: "./img/wordpress/wordpress6.jpg",
        title: "CREATIVE DESIGN",
        category: _CONTAINERS[Math.floor(Math.random() * (_CONTAINERS.length))]
    },
    {
        imageSource: "./img/wordpress/wordpress7.jpg",
        title: "CREATIVE DESIGN",
        category: _CONTAINERS[Math.floor(Math.random() * (_CONTAINERS.length))]
    },
    {
        imageSource: "./img/wordpress/wordpress8.jpg",
        title: "CREATIVE DESIGN",
        category: _CONTAINERS[Math.floor(Math.random() * (_CONTAINERS.length))]
    },
    {
        imageSource: "./img/wordpress/wordpress9.jpg",
        title: "CREATIVE DESIGN",
        category: _CONTAINERS[Math.floor(Math.random() * (_CONTAINERS.length))]
    },
    {
        imageSource: "./img/wordpress/wordpress10.jpg",
        title: "CREATIVE DESIGN",
        category: _CONTAINERS[Math.floor(Math.random() * (_CONTAINERS.length))]
    },

   ]);
const ButtonLoadText = document.createElement('div');
const containerImg = document.querySelector('.container-img');
const plusImg = document.createElement('img');
const ButtonLoad = document.createElement('button');

ButtonLoad.href = '#';
ButtonLoadText.innerHTML = 'Load More';
ButtonLoad.append(plusImg, ButtonLoadText);
plusImg.src = './img/plus-icon.png';
ButtonLoad.classList.add('load-button');
let indexOfGallery = null,
    currentCategory = null,
    displayedImageCount = 1,
    imageIndex = 0;
const filterList = document.querySelector('.filterList'),
    filterListItems = Array.from(document.querySelectorAll('.filterList-item')),
    gallery = document.querySelector('.filterList-gallery');
for (let i = 1; i < filterListItems.length; i++) {
    filterListItems[i].dataset.category = _CONTAINERS[i - 1];
}

filterList.addEventListener('click', (event) => {
    if (indexOfGallery) {
        filterListItems[indexOfGallery].classList.remove('filterList-item-active');
    } else {
        filterListItems[0].classList.remove('filterList-item-active')
    }
    indexOfGallery = filterListItems.indexOf(event.target);
    filterListItems[indexOfGallery].classList.add('filterList-item-active');

    let type = event.target.dataset.category;
    let imagesToRender = filterImages(imageList, type);

    if (imagesToRender.length <= 12) {
        ButtonLoad.style.display = 'none';
    } else {
        ButtonLoad.style.display = 'flex';
    }

    gallery.innerHTML = '';
    console.log(gallery);
    displayedImageCount = 1;
    imageIndex = 0;
    displayGallery(imagesToRender, currentCategory);
});

ButtonLoad.addEventListener('click', () => {

    const loader = document.createElement('div');
    loader.classList.add('loader');

    ButtonLoad.replaceWith(loader);

    setTimeout(() => {
        loader.replaceWith(ButtonLoad);
        displayGallery(filterImages(imageList, currentCategory));
        if (displayedImageCount >= 36) {
            ButtonLoad.remove();
        }
    }, 2000);

});


displayGallery(filterImages(imageList));


function filterImages(images, type) {
    let filteredImages = [];

    if (type) {
        currentCategory = type;
        images.forEach(image => {
            const category = image.category;

            if (type === category) {
                filteredImages.push(image);
            }
        });

    } else {
        currentCategory = type;
        return images;
    }

    return filteredImages;

}


function displayGallery(imagesToRender) {


    let displayedImageCountOnSingleClick = 1;


    for (imageIndex; imageIndex < imagesToRender.length; imageIndex++) {
        if (displayedImageCount <= 36 && displayedImageCountOnSingleClick <= 12) {
            const {imageSource, title, category} = imagesToRender[imageIndex],
                listItem = document.createElement('div'),
                listHover = document.createElement('div'),
                listImage = document.createElement('img'),
                listTitle = document.createElement('p'),
                listCategory = document.createElement('p'),
                imageContainerHover = document.createElement('a'),
                linkContainerHover = document.createElement('a'),
                searchContainerHover = document.createElement('div'),
                linkImg = document.createElement('div'),
                searchImg = document.createElement('div');

            listImage.src = imageSource;
            listItem.classList.add('list-item');
            listImage.dataset.category = category;
            listTitle.innerHTML = title;
            listCategory.innerHTML = category;
            searchContainerHover.classList.add('hover-search-container');
            searchContainerHover.append(searchImg);
            linkImg.classList.add('hover-img', 'hover-img-link');
            searchImg.classList.add('hover-img', 'hover-img-search');
            linkContainerHover.href = '#';
            imageContainerHover.href = '#';
            listImage.classList.add('list-image');
            listHover.classList.add('list-hover');
            listTitle.classList.add('list-title');
            listCategory.classList.add('list-category');
            imageContainerHover.append(
                linkContainerHover,
                searchContainerHover
            );
            listHover.append(
                imageContainerHover,
                listTitle,
                listCategory
            );

            linkContainerHover.classList.add('hover-link-container');
            linkContainerHover.append(linkImg);

            listItem.append(
                listImage
            );


            listItem.addEventListener('mouseenter', () => {
                listItem.append(listHover);

                listItem.addEventListener('mouseleave', () => {
                    listHover.remove();
                });
            });

            gallery.append(listItem);
            displayedImageCountOnSingleClick++;
            displayedImageCount++;
        } else {
            break;
        }
    }


    containerImg.append(gallery, ButtonLoad);
}
